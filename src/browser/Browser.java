package browser;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.WebDriverWait;

import def.Environment;
import util.Properties;

/**
 * ブラウザクラス.
 *
 */
public class Browser {

	/**
	 * ブラウザ型.
	 */
	private BrowserType type;

	/**
	 * ウェブドライバ.
	 */
	private WebDriver driver;

	/**
	 * コンストラクタ.
	 */
	public Browser() {
		this(BrowserType.valueOf(Properties.getString("browser.name")));
	}

	/**
	 * コンストラクタ.
	 */
	public Browser(BrowserType type) {
		this.type = type;
	}

	/**
	 * ブラウザを開始する.
	 */
	public void open() {
		System.setProperty(type.getDriverKey(), type.getDriverPath());
		try {
			driver = type.getDriverClass().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException("ドライバインスタンスが作成できない.", e);
		}
	}

	/**
	 * ページを遷移する.
	 *
	 * @param page
	 *            ページオブジェクト
	 */
	public void navigateTo(String url) {
		getDriver().navigate().to(url);
	}

	/**
	 * URLを取得する.
	 *
	 * @return URL
	 */
	public String getUrl() {
		return getDriver().getCurrentUrl();
	}

	/**
	 * タイトルを取得する.
	 *
	 * @return ページタイトル
	 */
	public String getTitle() {
		return getDriver().getTitle();
	}

	/**
	 * 要素が取得できるまで待機する.
	 *
	 * @param selector
	 *            要素指定子
	 * @return 取得要素
	 */
	public WebElement getElement(By selector) {
		return getElement(selector, Properties.getInt("page.load.timeout"));
	}

	/**
	 * 要素が取得できるまで待機する.
	 *
	 * @param selector
	 *            要素指定子
	 * @param timeout
	 *            最大待機秒
	 * @return 取得要素
	 */
	public WebElement getElement(By selector, int timeout) {
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		return wait.until(d -> d.findElement(selector));
	}

	/**
	 * ブラウザを閉じる.
	 */
	public void close() {
		getDriver().quit();
	}

	/**
	 * スクリーンショットを取得する.
	 * @throws IOException
	 * @throws WebDriverException
	 */
	public void takeScreenShot() throws WebDriverException, IOException {
		takeScreenShot(Environment.getEvidencePath());
	}

	/**
	 * スクリーンショットを取得する.
	 * @throws IOException
	 * @throws WebDriverException
	 */
	public void takeScreenShot(String path) throws WebDriverException, IOException {
		TakesScreenshot ts = (TakesScreenshot) new Augmenter().augment(driver);
		FileUtils.moveFile(ts.getScreenshotAs(OutputType.FILE), new File(path));
	}

	/**
	 * ドライバを取得するヘルパーメソッド. もし{@code driver == null }だった場合、例外が発生.
	 *
	 * @return ウェブドライバ
	 */
	private WebDriver getDriver() {
		if (driver == null)
			throw new IllegalStateException("WebDriverが初期されていない.");
		return driver;
	}

}
