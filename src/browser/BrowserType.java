package browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

/**
 * ブラウザ型定義.
 *
 */
public enum BrowserType {

	/**
	 * FireFox.
	 */
	Firefox(FirefoxDriver.class, "webdriver.gecko.driver", "./driver/geckodriver.exe"),
	/**
	 * Chrome.
	 */
	Chrome(ChromeDriver.class, "webdriver.chrome.driver", "./driver/chromedriver.exe"),
	/**
	 * IE.
	 */
	IE(InternetExplorerDriver.class, "webdriver.ie.driver", "./driver/IEDriverServer.exe");

	/**
	 * ドライバクラス.
	 */
	private Class<? extends WebDriver> driverClass;

	/**
	 * ドライバプロパティキー.
	 */
	private String driverKey;

	/**
	 * ドライバパス.
	 */
	private String driverPath;

	/**
	 * コンストラクタ.
	 * @param driverClass ドライバクラス
	 * @param driverKey パス設定キー
	 * @param driverPath ドライバパス
	 */
	private BrowserType(Class<? extends WebDriver> driverClass, String driverKey, String driverPath) {
		this.driverClass = driverClass;
		this.driverKey = driverKey;
		this.driverPath = driverPath;
	}

	/**
	 * ドライバクラス取得.
	 * @return ドライバクラス
	 */
	public Class<? extends WebDriver> getDriverClass() {
		return driverClass;
	}

	/**
	 * ドライバクラス設定キー取得.
	 * @return ドライバクラス設定キー
	 */
	public String getDriverKey() {
		return driverKey;
	}

	/**
	 * ドライバまでのパス取得.
	 * @return ドライバまでのパス
	 */
	public String getDriverPath() {
		return driverPath;
	}


}
