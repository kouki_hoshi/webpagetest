package def;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.stream.Stream;

import util.Properties;

/**
 * 環境定義.
 *
 */
public class Environment {

	/**
	 * インスタンス化禁止.
	 */
	private Environment() {
	}

	/**
	 * ベースURLを取得する.
	 *
	 * @return ベースURL
	 */
	public static String getBaseUrl() {
		return Properties.getString("root.url");
	}

	/**
	 * 呼び出し元のテストメソッドを特定する.
	 *
	 * @return テストメソッドトレース
	 */
	public static String getEvidenceRoot() {
		String root = Properties.getString("evidence.root");
		if (root.endsWith("/") || root.endsWith("\\")) {
			return root;
		}
		return root + "/";
	}

	/**
	 * 呼び出し元のテストメソッドを特定する.
	 *
	 * @return テストメソッドトレース
	 */
	public static String getEvidencePath() {
		StackTraceElement ste = getTestMethod();
		return getEvidenceRoot() + getEvidenceDir(ste) + getEvidenceFile(ste);
	}

	/**
	 * 呼び出し元テストクラスおよびそのメソッド名からディレクトリパスを取得する.
	 *
	 * @return エビデンスディレクトリ
	 */
	private static String getEvidenceDir(StackTraceElement ste) {
		return ste.getClassName().replace(".", "/") + "/";
	}

	/**
	 * 呼び出し元テストクラスおよびそのメソッド名からパスを作成する.
	 *
	 * @return エビデンスファイル名
	 */
	private static String getEvidenceFile(StackTraceElement ste) {
		String dateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("_yyyyMMdd_hhmmss_SSS"));
		return ste.getMethodName() + dateTime + ".png";
	}

	/**
	 * 呼び出し元のテストメソッド情報を取得する.
	 *
	 * @return テストメソッドトレース
	 */
	private static StackTraceElement getTestMethod() {
		Stream<StackTraceElement> traces = Arrays.stream(Thread.currentThread().getStackTrace());
		return traces.filter(st -> st.getClassName().endsWith("Test")).findFirst().get();
	}

}
