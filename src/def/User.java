package def;

import util.Properties;

/**
 * ユーザ情報.
 *
 */
public enum User {

	/**
	 * 申請者.
	 */
	APPLICANT("user0.id", "user0.name", "user0.password"),
	/**
	 * 第一承認者.
	 */
	FIRST_AUTH("user1.id", "user1.name", "user1.password"),
	/**
	 * 第二承認者.
	 */
	SECOND_AUTH("user2.id", "user2.name", "user2.password"),
	/**
	 * 不正なパスワード.
	 */
	INVALID("invalid.id", "invalid.name", "invalid.password");

	/**
	 * ID.
	 */
	private String id;

	/**
	 * 名前.
	 */
	private String name;

	/**
	 * パスワード.
	 */
	private String password;

	/**
	 * コンストラクタ.
	 * @param idKey IDキー
	 * @param nameKey 名前キー
	 * @param passwordKey パスワードキー.
	 */
	private User(String idKey, String nameKey, String passwordKey) {
		this.id = Properties.getString(idKey);
		this.name = Properties.getString(nameKey);
		this.password = Properties.getString(passwordKey);
	}

	/**
	 * IDの取得.
	 *
	 * @return ID
	 */
	public String getId() {
		return id;
	}

	/**
	 * 名前の取得.
	 *
	 * @return パスワード
	 */
	public String getName() {
		return name;
	}

	/**
	 * パスワードの取得.
	 *
	 * @return パスワード
	 */
	public String getPassword() {
		return password;
	}

}
