package page;

import org.openqa.selenium.By;

/**
 * ログイン画面.
 *
 */
public class LoginPage extends Page {

	/**
	 * ID テキスト.
	 */
	public By idText = By.id("WebUserControl11_UserName");

	/**
	 * パスワードテキスト.
	 */
	public By passwordText = By.id("WebUserControl11_Password");

	/**
	 * ログインボタン.
	 */
	public By submitButton = By.id("WebUserControl11_SubmitButton");

	/**
	 * エラーコメント.
	 */
	public By errorComment = By.id("WebUserControl11_MessageLabel");

	@Override
	public String getTitle() {
		return "";
	}

	@Override
	public String getUrl() {
		return "http://localhost/LoginForm/LoginForm/LoginForm.aspx";
	}
}
