package page;

/**
 * 抽象ページ.
 *
 */
public abstract class Page {

	/**
	 * URLを取得する.
	 * @param params パスパラメータ
	 * @return パス
	 */
	public abstract String getUrl();

	/**
	 * ページタイトルを取得する.
	 *
	 * @param params パスパラメータ
	 * @return パス
	 */
	public abstract String getTitle();

}
