package util;

import java.util.ResourceBundle;

/**
 * プロパティ取得用.
 *
 */
public class Properties {

	/**
	 * インスタンス化禁止.
	 */
	private Properties() {
	}

	/**
	 * リソースバンドルの保持.
	 */
	private static ResourceBundle bundle = ResourceBundle.getBundle("test");

	/**
	 * プロパティから文字データを取得する.
	 * @param key キー
	 * @return 値
	 */
	public static String getString(String key) {
		return bundle.getString(key);
	}

	/**
	 * プロパティからInt値を取得する.
	 * @param key キー
	 * @return 値
	 */
	public static int getInt(String key) {
		return Integer.parseInt(getString(key));
	}

}
