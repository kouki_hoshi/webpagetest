package page;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import browser.Browser;
import def.User;
import rule.LoginRule;
import rule.WebTestRule;

/**
 * ログインテスト.
 *
 */
public class LoginPageTest {

	/**
	 * テスト対象.
	 */
	static LoginPage loginPage = new LoginPage();

	public static class ログイン状態 {

		@Rule
		public LoginRule rule = new LoginRule();

		/**
		 * ログイン状態ではメンバーページに遷移すること.
		 *
		 * @throws Exception
		 *             テスト中の例外
		 */
		@Test
		public void メンバーページに遷移すること() throws Exception {
			rule.getBrowser().navigateTo(loginPage.getUrl());
		}
	}

	public static class ログイン前状態 {

		@Rule
		public WebTestRule rule = new WebTestRule();

		/**
		 * ブラウザ.
		 */
		Browser browser;

		/**
		 * テスト前に実施する内容.
		 */
		@Before
		public void setUp() {
			browser = rule.getBrowser();
			browser.navigateTo(loginPage.getUrl());
		}

		/**
		 * ログインテスト.
		 *
		 * @throws Exception
		 *             テスト中の例外
		 */
		@Test
		public void ログインテスト() throws Exception {
			User user = User.APPLICANT;
			browser.getElement(loginPage.idText).sendKeys(user.getId());
			browser.getElement(loginPage.passwordText).sendKeys(user.getPassword());
			browser.getElement(loginPage.submitButton).click();

			assertThat(browser.getUrl(), is(new MemberPage().getUrl()));
		}

		/**
		 * 認証失敗テスト.
		 *
		 * @throws Exception
		 *             テスト中の例外
		 */
		@Test
		public void ログインが失敗すること() throws Exception {
			User user = User.INVALID;
			browser.getElement(loginPage.idText).sendKeys(user.getId());
			browser.getElement(loginPage.passwordText).sendKeys(user.getPassword());
			browser.getElement(loginPage.submitButton).click();

			assertThat(browser.getUrl(), is(loginPage.getUrl()));
			assertThat(browser.getElement(loginPage.errorComment).getText(), is("登録されていませんよ？"));
		}

	}

}
