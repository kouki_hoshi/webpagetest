package rule;

import def.User;
import page.LoginPage;

public class LoginRule extends WebTestRule {

	@Override
	protected void before() throws Throwable {
		super.before();
		super.browser.navigateTo(new LoginPage().getUrl());
		super.browser.getElement(new LoginPage().idText).sendKeys(User.APPLICANT.getId());
		super.browser.getElement(new LoginPage().passwordText).sendKeys(User.APPLICANT.getPassword());
		super.browser.getElement(new LoginPage().submitButton).click();
	}

	@Override
	protected void after() {
		browser.close();
	}

}
