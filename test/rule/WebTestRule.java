package rule;

import org.junit.rules.ExternalResource;

import browser.Browser;

public class WebTestRule extends ExternalResource {

	protected Browser browser = new Browser();

	public Browser getBrowser() {
		return browser;
	}

	@Override
	protected void before() throws Throwable {
		browser.open();
	}

	@Override
	protected void after() {
		browser.close();
	}

}
